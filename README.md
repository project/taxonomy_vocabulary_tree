# Taxonomy vocabulary tree

Provides functionality for build a block with a taxonomy tree with a custom template for links.

## Installation
Composer must be used.

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229), use Composer to
download the module:
   ```sh
   composer require "drupal/taxonomy_tree_block"
   ```
