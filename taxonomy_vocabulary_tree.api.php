<?php

/**
 * Modify the taxonomy vocabulary ree.
 *
 * @param array $tree
 *   The tree with terms
 * @param array $vocabulary_name
 *   The vocabulary name for tree.
 */
function hook_taxonomy_vocabulary_tree_alter(array &$tree, $vocabulary_name) {
  // Change the 'tree'.
}
