<?php

namespace Drupal\taxonomy_vocabulary_tree\Service;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\taxonomy_vocabulary_tree\LazyBlockInterface;

/**
 * Class BlockLazyBuilder.
 */
class BlockLazyBuilder {

  /**
   * The block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $blockStorage;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The Vocabulary Tree Repository service.
   *
   * @var \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository
   */
  protected $taxonomyVocabularyTreeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Creates a new LazyBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository $taxonomy_vocabulary_tree_manager
   *   The Vocabulary Tree Repository service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    BlockManagerInterface $block_manager,
    VocabularyTreeRepository $taxonomy_vocabulary_tree_manager,
    AccountInterface $current_user
  ) {
    $this->blockManager = $block_manager;
    $this->blockStorage = $entity_type_manager->getStorage('block');
    $this->taxonomyVocabularyTreeManager = $taxonomy_vocabulary_tree_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Renderer block.
   *
   * @param string $block_entity_id
   *   The block entity id.
   *
   * @return array
   *   The renderable array with the block.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function lazyRenderBlock(string $block_entity_id) {
    $build = [];
    /** @var \Drupal\block\Entity\Block $block */
    $block = $this->blockStorage->load($block_entity_id);
    $plugin_block = $block->getPlugin();
    if ($plugin_block instanceof LazyBlockInterface) {
      $access_result = $plugin_block->access($this->currentUser);
      if (is_object($access_result) && $access_result->isForbidden() || is_bool($access_result) && !$access_result) {
        return [];
      }
      $build = $plugin_block->lazyBuild();
    }


    return $build;
  }

}
