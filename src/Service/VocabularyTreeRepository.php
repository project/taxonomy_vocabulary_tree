<?php

namespace Drupal\taxonomy_vocabulary_tree\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class VocabularyTreeRepository.
 */
class VocabularyTreeRepository {

  const USED_TERM = 'used_term';

  const HAS_USED_TERM_CHILD = 'has_used_term_child';

  const NOT_USED_TERM = 'not_used_term';

  /**
   * Term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The database connection to be used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates a new VocabularyTreeService.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    ModuleHandlerInterface $module_handler,
    Connection $database
  ) {
    $this->termStorage = $entity_type_manager->getStorage(
      'taxonomy_term'
    );
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
    $this->database = $database;
  }

  /**
   * Load hierarchy tree in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree. If 0, generate the tree
   *   for the entire vocabulary.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   * @param string $langcode
   *   The langcode to translate the terms.
   * @param bool $only_used_terms
   *   TRUE if you want to load only used terms by the taxonomy_index.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   An array of term objects that are the children of the vocabulary $vid.
   */
  public function loadHierarchyTree(
    string $vid,
    int $parent = 0,
    int $max_depth = NULL,
    string $langcode = '',
    bool $only_used_terms = FALSE
  ) {
    $terms = $this->termStorage->loadTree(
      $vid,
      $parent,
      $max_depth,
      TRUE
    );
    $translated_terms = $this->getTermTranslations($terms, $langcode);
    $structured_tree = $this->structuringTree($translated_terms);
    $tree_with_used_status = $this->treeUsedStatusProcess($structured_tree);
    if ($only_used_terms) {
      $tree = $this->filterNotUsedTerms($tree_with_used_status);
    }
    else {
      $tree = $tree_with_used_status;
    }

    $this->moduleHandler->alter(
      'taxonomy_vocabulary_tree',
      $tree,
      $vid
    );

    return $tree;
  }

  /**
   * Filter not used terms.
   *
   * @param array $tree
   *   A nested set array of taxonomy terms with children.
   *
   * @return array
   *   The filtered tree.
   */
  protected function filterNotUsedTerms(array $tree) {
    $filtered_tree = [];

    foreach ($tree as $tid => $element) {
      if ($element['used_status'] !== self::NOT_USED_TERM) {
        $filtered_tree[$tid]['entity'] = $element['entity'];
        if (!empty($element['children'])) {
          $filtered_tree[$tid]['children'] = $this->filterNotUsedTerms($element['children']);
        }
      }
    }

    return $filtered_tree;
  }

  /**
   * Set a used status for elements.
   *
   * @param array $tree
   *   A nested set array of taxonomy terms with children.
   *
   * @return array
   *   The tree with a recorded used status.
   */
  protected function treeUsedStatusProcess(array $tree) {
    foreach ($tree as $tid => $element) {
      $tree[$tid] = $this->termUsedStatusProcess($element);
    }

    return $tree;
  }

  /**
   * Term Check Is Used Process.
   *
   * @param array $element
   *   The term entity with children.
   *
   * @return mixed
   *   The element with recorded a used status.
   */
  protected function termUsedStatusProcess(array $element) {
    if (!empty($element['children'])) {
      foreach ($element['children'] as $child_tid => $child) {
        $element['children'][$child_tid] = $this->termUsedStatusProcess($child);
      }
    }

    $element['used_status'] = $this->getUsedStatus($element);

    return $element;
  }

  /**
   * Get element used status.
   *
   * @param array $element
   *   The element with term entity and children.
   *
   * @return string
   *   The used status.
   */
  protected function getUsedStatus(array $element) {
    $vid = $element['entity']->bundle();
    $tid = $element['entity']->id();
    $used_tids = $this->getUsedTermIdsByVocabulary($vid);
    if (in_array($tid, $used_tids)) {
      return self::USED_TERM;
    }
    foreach ($element['children'] as $child) {
      if (in_array($child['used_status'], [self::USED_TERM, self::HAS_USED_TERM_CHILD], TRUE)) {
        return self::HAS_USED_TERM_CHILD;
      }
    }

    return self::NOT_USED_TERM;
  }

  /**
   * Get used term IDs by vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID.
   *
   * @return mixed
   *   An array with used terms IDs.
   */
  public function getUsedTermIdsByVocabulary(string $vid) {
    static $used_tids;

    if (isset($used_tids[$vid])) {
      return $used_tids[$vid];
    }
    $query = $this->database->select('taxonomy_entity_index', 'ti');
    $query->leftJoin($this->termStorage->getBaseTable(), 'td', 'ti.tid = td.tid');
    $query->condition('td.vid', $vid);
    $query->fields('ti', ['tid']);
    $used_tids[$vid] = $query->execute()->fetchCol();

    return $used_tids[$vid];
  }

  /**
   * Finds all translated terms in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree. If 0, generate the tree
   *   for the entire vocabulary.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   * @param string $langcode
   *   The langcode to translate the terms.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   An array of term objects that are the children of the vocabulary $vid.
   */
  public function loadTree(
    string $vid,
    int $parent = 0,
    int $max_depth = NULL,
    string $langcode = ''
  ) {
    $terms = $this->termStorage->loadTree(
      $vid,
      $parent,
      $max_depth,
      TRUE
    );

    return $this->getTermTranslations($terms, $langcode);
  }

  /**
   * Finds all children of a term ID.
   *
   * The children will be translated to current language.
   *
   * @param int $parent_id
   *   Term ID to retrieve children for.
   * @param string $vid
   *   An optional vocabulary ID to restrict the child search.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   An array of translated term objects
   *   that are the children of the term $tid.
   */
  public function loadChildren(int $parent_id, string $vid) {
    $terms = $this->termStorage->loadChildren($parent_id, $vid);

    return $this->getTermTranslations($terms);
  }

  /**
   * Translating terms.
   *
   * @param array $terms
   *   A nested set array of taxonomy terms with children.
   * @param string $langcode
   *   The langcode of the language to use.
   *
   * @return \Drupal\taxonomy\Entity\Term[]
   *   The translated terms.
   */
  public function getTermTranslations(array $terms, string $langcode = '') {
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }
    $translated_terms = [];

    /** @var \Drupal\taxonomy\Entity\Term $term */
    foreach ($terms as $term) {
      if ($term->hasTranslation($langcode)) {
        $translated_terms[$term->id()] = $term->getTranslation($langcode);
      }
      else {
        $translated_terms[$term->id()] = $term;
      }
    }

    return $translated_terms;
  }

  /**
   * Structuring vocabulary tree.
   *
   * @param array $terms
   *   The terms.
   *
   * @return array
   *   The structured tree.
   */
  protected function structuringTree(array $terms) {
    $tree = [];
    $depth = 0;
    $links = [0 => &$tree];
    $link = $links[0];
    foreach ($terms as $term) {
      $parent_id = $term->get('parent')->getString();
      if ($term->depth > $depth) {
        $link = &$link['children'][$parent_id];
        $links[$parent_id] = &$link;
      }
      $depth = $term->depth;

      unset($link);
      $link = &$links[$parent_id];
      $link['children'][$term->id()]['entity'] = $term;
    }

    return $tree['children'];
  }

}
