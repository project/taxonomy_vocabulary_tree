<?php

namespace Drupal\taxonomy_vocabulary_tree;

/**
 * Interface LazyBlockInterface.
 */
interface LazyBlockInterface {

  /**
   * Lazy render the block.
   *
   * @return array
   *   The renderable array with the block.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function lazyBuild();

}
