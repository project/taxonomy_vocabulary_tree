<?php

namespace Drupal\taxonomy_vocabulary_tree\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the color_field box widget.
 *
 * @FieldWidget(
 *   id = "taxonomy_vocabulary_tree_widget",
 *   module = "taxonomy_vocabulary_tree",
 *   label = @Translation("Taxonomy vocabulary tree widget"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class TaxonomyVocabularyTreeWidget extends OptionsWidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The taxonomy_vocabulary_tree.repository service.
   *
   * @var \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository
   */
  protected $vocabularyTree;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs a MediaLibraryWidget widget.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository $vocabulary_tree
   *   The taxonomy_vocabulary_tree.repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    VocabularyTreeRepository $vocabulary_tree,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->vocabularyTree = $vocabulary_tree;
    $this->termStorage = $entity_type_manager->getStorage(
      'taxonomy_term'
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /* @noinspection  PhpParamsInspection */
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('taxonomy_vocabulary_tree.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $settings = $items->getItemDefinition()->getSettings();
    $element['#type'] = 'container';
    $element['#id'] = $items->getName() . '-tree-wrapper';

    foreach ($settings["handler_settings"]["target_bundles"] as $vocabulary_name) {
      $element[$vocabulary_name] = [
        '#type' => 'container',
      ];

      $trigger = $form_state->getTriggeringElement();
      if (empty($trigger)) {
        $selected = $this->getSelectedOptions($items);
        $selected_values = array_combine($selected, $selected);
      }
      else {
        $values = $form_state->getValues()[$items->getName()] ?? [];
        $selected_values = array_column($values, 'target_id', 'target_id');
      }
      $element[$vocabulary_name] += $this->buildDepth(
        0,
        $vocabulary_name,
        $selected_values
      );
    }
    $element['#attached']['library'][] = 'taxonomy_vocabulary_tree/tree-widget';

    return $element;
  }

  /**
   * Ajax callback for refresh form.
   *
   * @param array $form
   *   The form structure where field elements are attached to. This might be a
   *   full form structure, or a sub-element of a larger form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The renderable array of field.
   */
  public function ajaxChildTerms(
    array &$form,
    FormStateInterface $form_state
  ) {
    $field_name = $this->fieldDefinition->getName();

    return $form[$field_name];
  }

  /**
   * Build depth renderable array.
   *
   * @param int $parent_id
   *   The term id of parent term.
   * @param string $vid
   *   The vocabulary name.
   * @param array $selected_values
   *   The selected values.
   *
   * @return array
   *   The renderable array.
   */
  public function buildDepth(int $parent_id, string $vid, array $selected_values) {
    $depth = [
      '#type' => 'container',
      '#attributes' => ['class' => 'depth-wrapper'],
    ];
    $options = $this->loadOptions($parent_id, $vid);
    if (empty($options)) {
      return $depth;
    }
    $selected_options = array_intersect_key($selected_values, $options);
    $depth['terms'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $selected_options,
    ];
    if ($parent_id == 0) {
      $depth['terms']['#title'] = $this->fieldDefinition->getLabel();
    }
    else {
      /** @var \Drupal\taxonomy\Entity\Term $parent_term */
      $parent_term = $this->termStorage->load($parent_id);
      $depth['terms']['#title'] = $parent_term->getName();
    }

    $children_options = [];
    foreach ($options as $term_id => $term_name) {
      $children_options[$term_id] = $this->loadOptions($term_id, $vid);
    }
    if (!empty(array_filter($children_options))) {
      $depth['terms']['#ajax'] = [
        'callback' => [$this, 'ajaxChildTerms'],
        'wrapper' => $this->fieldDefinition->getName() . '-tree-wrapper',
      ];
    }

    $depth['children'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'category-children-wrapper'],
    ];
    foreach ($selected_options as $term_id) {
      $depth['children'][$term_id] = $this->buildDepth(
        $term_id,
        $vid,
        $selected_values
      );
    }

    return $depth;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(
    array $element,
    FormStateInterface $form_state
  ) {
    $form_values = $form_state->getValues()[$element["#field_name"]];
    $values = [];
    foreach ($form_values as $depth) {
      $values = self::getDepthValues($depth);
    }

    // Filter out the 'none' option. Use a strict comparison, because
    // 0 == 'any string'.
    $index = array_search('_none', $values, TRUE);
    if ($index !== FALSE) {
      unset($values[$index]);
    }

    // Transpose selections from field => delta to delta => field.
    $items = [];
    foreach ($values as $value) {
      $items[] = [$element['#key_column'] => $value];
    }
    $form_state->setValueForElement($element, $items);
  }

  /**
   * Exstract values from form_state values.
   *
   * @param array $depth
   *   The field in form_state values.
   *
   * @return array
   *   The values.
   */
  protected static function getDepthValues(array $depth) {
    $values = [];
    if (!empty($depth['terms'])) {
      $depth['terms'] = array_filter($depth['terms']);
      $values = array_merge($values, $depth['terms']);
    }
    if (!empty($depth["children"])) {
      foreach ($depth["children"] as $child_depth) {
        $values = array_merge($values, self::getDepthValues($child_depth));
      }
    }

    return $values;
  }

  /**
   * Load the options for one subcategory.
   *
   * @param int $parent_id
   *   The term id.
   * @param string $vocabulary_name
   *   The vocabulary machine name.
   *
   * @return array
   *   The options for one subcategory.
   */
  protected function loadOptions(int $parent_id, string $vocabulary_name) {
    if ($parent_id == 0) {
      $terms = $this->vocabularyTree->loadTree($vocabulary_name, $parent_id, 1);
    }
    else {
      $terms = $this->vocabularyTree->loadChildren($parent_id, $vocabulary_name);
    }

    $options = [];
    foreach ($terms as $term) {
      $options[$term->id()] = $term->getName();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if (!$this->required && !$this->multiple) {
      return $this->t('N/A');
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(
    FieldDefinitionInterface $field_definition
  ) {
    $item_definition = $field_definition->getItemDefinition();

    $type = $item_definition->getDataType();
    $is_multiple = $field_definition->getFieldStorageDefinition()->isMultiple();

    if ($type == 'field_item:entity_reference' && $is_multiple) {
      $target_type = $item_definition->getSetting('target_type');
      if ($target_type == 'taxonomy_term') {
        return TRUE;
      }
    }

    return FALSE;
  }

}
