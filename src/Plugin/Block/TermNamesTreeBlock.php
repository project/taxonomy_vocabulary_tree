<?php

namespace Drupal\taxonomy_vocabulary_tree\Plugin\Block;

use Drupal\block\BlockForm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy_vocabulary_tree\LazyBlockInterface;
use Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block.
 *
 * @Block(
 *  id = "taxonomy_vocabulary_tree_block",
 *  admin_label = @Translation("Taxonomy vocabulary tree block"),
 * )
 */
class TermNamesTreeBlock extends BlockBase implements ContainerFactoryPluginInterface, LazyBlockInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Vocabulary Tree Repository service.
   *
   * @var \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository
   */
  protected $taxonomyVocabularyTreeManager;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SystemBreadcrumbBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\taxonomy_vocabulary_tree\Service\VocabularyTreeRepository $taxonomy_vocabulary_tree_manager
   *   The Vocabulary Tree Repository service.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    VocabularyTreeRepository $taxonomy_vocabulary_tree_manager,
    CurrentPathStack $current_path,
    LanguageManagerInterface $language_manager,
    RouteMatchInterface $route_match,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->taxonomyVocabularyTreeManager = $taxonomy_vocabulary_tree_manager;
    $this->currentPath = $current_path;
    $this->languageManager = $language_manager;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /*  @noinspection  PhpParamsInspection */
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('taxonomy_vocabulary_tree.repository'),
      $container->get('path.current'),
      $container->get('language_manager'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function build() {
    $config = $this->getConfiguration();
    $build['content'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['taxonomy-menu-tree']],
      'block' => [
        '#lazy_builder' => [
          'taxonomy_vocabulary_tree.lazy_builder:lazyRenderBlock',
          [$config['block_entity_id']],
        ],
        '#create_placeholder' => TRUE,
        '#cache' => [
          'max-age' => 0,
        ],
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function lazyBuild() {
    $config = $this->getConfiguration();
    if (empty($config['vocabulary_name'])) {
      return $this->warningsMessage();
    }
    $tree = $this->taxonomyVocabularyTreeManager->loadHierarchyTree(
      $config['vocabulary_name'],
      0,
      NULL,
      '',
      $config['show_only_used_terms']
    );
    if (empty($tree)) {
      $build = [
        'content' => [
          '#markup' => $this->t('Empty vocabulary'),
        ],
      ];
    }
    else {
      $build = $this->buildTerms($tree);
    }
    $build['#attached']['library'][] = 'taxonomy_vocabulary_tree/menu-tree';

    return $build;
  }

  /**
   * Show warning message.
   *
   * @return array
   *   The status messages.
   */
  protected function warningsMessage() {
    $this->messenger()->addWarning(
      $this->t('Please select vocabulary_name in configuration block.')
    );

    return [
      '#type' => 'status_messages',
    ];
  }

  /**
   * Build terms.
   *
   * @param array $terms
   *   The term.
   *
   * @return array
   *   The renderable array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function buildTerms(array $terms) {
    $menu_classes = $this->getConfiguration()['menu_classes'] ?? '';

    $build = [
      '#type' => 'container',
      '#theme_wrappers' => [],
      '#prefix' => '<ul class="' . $menu_classes . ' menu-tree-main-level menu-tree-nested">',
      '#suffix' => '</ul>',
    ];

    foreach ($terms as $term) {
      $build[] = $this->buildTerm($term);
    }

    return $build;
  }

  /**
   * Build term.
   *
   * @param array $term
   *   The term.
   *
   * @return array
   *   Renderable array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function buildTerm(array $term) {
    $is_current = $this->isCurrentTerm($term['entity']);
    $config = $this->getConfiguration();
    $build = [
      '#type' => 'inline_template',
      '#template' => '<li class="{{ item_classes }} without-nesting"> {{ html_prefix }}  {{ term_name }} </li>',
      '#context' => [
        'item_classes' => $config['item_classes'] ?? '',
        'html_prefix' => [
          '#markup' => $config['without_nesting_html_prefix'] ?? '',
        ],
        'term_name' => [
          '#markup' => $this->buildTermName($term['entity']),
        ],
      ],
    ];

    if (empty($term['children'])) {
      $build['#attributes']['data-child-is-active'] = $is_current ? 'active' : 'not-active';

      return $build;
    }
    else {
      $children = [];
      $opened = FALSE;
      foreach ($term['children'] as $child) {
        $child_build = $this->buildTerm($child);
        $children[] = $child_build;
        if ($is_current || $child_build['#attributes']['data-child-is-active'] == 'active') {
          $opened = TRUE;
        }
      }

      $build['#template'] = '<li class="{{ item_classes }}"> {{ html_prefix }}  {{ term_name }}';
      if ($opened) {
        $build['#template'] .= ' <ul class="menu-tree-nested active">';
      }
      else {
        $build['#template'] .= ' <ul class="menu-tree-nested">';
      }
      $build['#context']['html_prefix']['#markup'] = $config['html_prefix'] ?? '';

      return [
        '#type' => 'container',
        '#theme_wrappers' => [],
        'item' => $build,
        'children' => $children,
        '#suffix' => '</ul></li>',
        // If the children are opened then make the all parrent opened also.
        '#attributes' => ['data-child-is-active' => $opened ? 'active' : 'not-active'],
      ];
    }
  }

  /**
   * Check if  the term URL is current.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The taxonomy term.
   *
   * @return bool
   *   TRUE if the term URL is current, FALSE otherwise.
   */
  protected function isCurrentTerm(TermInterface $term) {
    $config = $this->getConfiguration();
    $current_term = $this->routeMatch->getParameter('taxonomy_term');
    if ($current_term instanceof TermInterface && $config['open_current_term']) {
      return $current_term->id() == $term->id();
    }

    return FALSE;
  }

  /**
   * Build term name.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The taxonomy term.
   *
   * @return string
   *   The term name link.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function buildTermName(TermInterface $term) {
    $term_url = $this->getTermUrl($term);

    return '<a href="' . $term_url . '">' . $term->getName() . '</a>';
  }

  /**
   * Get a Term Url.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The taxonomy term of the vocabulary.
   *
   * @return \Drupal\Core\GeneratedUrl|mixed|string
   *   The term URL.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getTermUrl(TermInterface $term) {
    $config = $this->getConfiguration();
    $use_url_from_term = $config['use_url_from_term'] ?? FALSE;

    if ($use_url_from_term) {
      return $term->toUrl()->toString();
    }
    else {
      $prefix = $this->getCurrentLanguagePrefix();
      $url_template = $config['term_url'] ? $prefix . $config['term_url'] : '';
      $url = str_replace('[tid]', $term->id(), $url_template);

      return URL::fromUserInput($url)->toString();
    }

  }

  /**
   * Get the current language prefix.
   *
   * @return string
   *   The current language prefix.
   */
  protected function getCurrentLanguagePrefix() {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $prefixes = \Drupal::config('language.negotiation')->get('url.prefixes');

    if (!empty($prefixes[$langcode])) {
      return '/' . $prefixes[$langcode];
    }

    return $prefixes[$langcode];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $vocabulary_names = taxonomy_vocabulary_get_names();

    $config = $this->getConfiguration();
    $form['vocabulary_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary name'),
      '#options' => $vocabulary_names,
      '#default_value' => $config['vocabulary_name'],
      '#required' => TRUE,
    ];

    $form['open_current_term'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open current term.'),
      '#default_value' => $config['open_current_term'],
      '#description' => $this->t('Open the menu item if the current path equals the term path from the menu item.'),
    ];

    $form['show_only_used_terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show only used terms."),
      '#default_value' => $config['show_only_used_terms'],
      '#description' => $this->t("Hide don't used terms."),
    ];

    $form['use_url_from_term'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use url from term entity.'),
      '#default_value' => $config['use_url_from_term'],
    ];

    $form['term_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Term url'),
      '#default_value' => $config['term_url'],
      '#maxlength' => 255,
      '#description' => $this->t('You can use [tid] for term id in url.'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="settings[use_url_from_term]"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $form['html_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Html prefix'),
      '#default_value' => $config['html_prefix'],
      '#maxlength' => 255,
      '#description' => $this->t('The html prefix before menu item.'),
    ];

    $form['without_nesting_html_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Without nesting html prefix'),
      '#default_value' => $config['without_nesting_html_prefix'],
      '#maxlength' => 255,
      '#description' => $this->t('The html prefix before menu item without nesting.'),
    ];

    $form['menu_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu classes'),
      '#default_value' => $config['menu_classes'],
      '#maxlength' => 255,
      '#description' => $this->t('The css classes of menu.'),
    ];

    $form['item_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu item classes'),
      '#default_value' => $config['item_classes'],
      '#maxlength' => 255,
      '#description' => $this->t('The css classes of menu item.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'vocabulary_name' => NULL,
      'open_current_term' => TRUE,
      'show_only_used_terms' => TRUE,
      'use_url_from_term' => TRUE,
      'term_url' => '/taxonomy/term/[tid]',
      'html_prefix' => '<i class="menu-tree-caret"></i>',
      'without_nesting_html_prefix' => '<i class="menu-tree-caret-without-nesting"></i>',
      'menu_classes' => 'list-group categories',
      'item_classes' => 'list-group-item',
      'block_entity_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof BlockForm) {
      $block_entity_id = $form_object->getEntity()->id();
      $this->setConfigurationValue('block_entity_id', $block_entity_id);
    }

    foreach ($form_state->getValues() as $name => $value) {
      $this->setConfigurationValue($name, $value);
    }
  }

}
