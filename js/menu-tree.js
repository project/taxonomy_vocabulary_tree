(function () {
  document.addEventListener('click', function (e) {
    if (e.target && e.target.nodeName === 'I') {
      let tree = e.target.closest('.menu-tree-main-level')
      if (tree !== null) {
        let item = e.target.parentElement.querySelector('.menu-tree-nested')
        if (item !== null) {
          item.classList.toggle('active')
        }
      }
    }
  })
})()
